# Inforack (in progress)

`Inforack` is a lightweight and intuitive tool for storing and organizing various types of resources such as websites, articles, blog posts, and more. It provides a centralized platform to save your favorite resources, categorize them based on topics or tags, and easily retrieve them when needed.
