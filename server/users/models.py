from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.db import models


class CustomUserManager(BaseUserManager):
    """A class to manage user accounts"""

    def create_user(self, email, username, password):
        """Creates and saves a user with the given email, username and password"""
        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )
        user.set_password(password)
        # save user to database
        user.save()
        return user

    def create_superuser(self, email, username, password):
        """Creates and saves a superuser with the given email, name and password"""
        user = self.create_user(
            email=email,
            username=username,
            password=password,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """A class to create a custom user account"""

    email = models.EmailField(max_length=254, unique=True)
    username = models.CharField(max_length=30, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.username

    class Meta:
        verbose_name_plural = 'users'